/* DEBUG CANNY
  filesystem::path images_p(options.images_directory);
  filesystem::directory_iterator iter(images_p);
  Mat img = imread ( iter->path().string(), cv::IMREAD_COLOR );
  if( img.empty())
    cout << "ERROR";

  resize(img , img , cv::Size(img_w, img_h));
  if( img.channels() == 1 )
    cvtColor ( img,  img, cv::COLOR_GRAY2BGR );
    

  CannyEdgeDetectorUniquePtr edge_detector_ptr( new CannyEdgeDetector() );
   
  while(true)
  {
    int tresh, ratio;
    cout << "Definisci thresh, ratio:" <<endl;
    cin >> tresh >> ratio;
    edge_detector_ptr->setLowThreshold(tresh);
    edge_detector_ptr->setRatio(ratio); 
     edge_detector_ptr->enableRGBmodality(true);
    edge_detector_ptr->enableWhiteBackground(true);
    edge_detector_ptr->setImage(img);
    Mat edge_map;
    edge_detector_ptr->getEdgeMap(edge_map);
    showImage(edge_map, "Edge map", true, 500);
  }
  //END_DEBUG CANNY
  */


/* DEBUG for too close matches
double prev_dist = (t_old - match.t_vec).norm();
cout << "Prev_dist: " << prev_dist << " - ";
if(prev_dist < 0.15)
{
    cout << endl;
    continue;
}
t_old = match.t_vec;
*/


/*Mask operation DEBUG
Mat mask = model_p->getMask();
Mat inside_edge = edge_map.clone();
cv_ext::drawSegments( mask, proj_seg, cv::Scalar ( 255,255,255),3);
inside_edge.setTo(cv::Scalar(0,0,0), mask);
namedWindow("Mask", CV_WINDOW_NORMAL);
resizeWindow("Mask", cam_model.imgWidth()*0.25, cam_model.imgHeight()*0.25);
showImage(inside_edge,"Mask",true, 100);
*/


/*Mask operation DEBUG
mask = m_p->getMask();
Mat maskC = mask(roi);
Mat inside_edgeC = edge_map(roi);
cv_ext::drawSegments( maskC, pj_seg, cv::Scalar ( 255,255,255),3);
inside_edgeC.setTo(cv::Scalar(0,0,0), maskC);
namedWindow("Mask", CV_WINDOW_NORMAL);
resizeWindow("Mask", cam_model.imgWidth()*0.25, cam_model.imgHeight()*0.25);
showImage(inside_edgeC,"Mask",true, 100); 
*/

///For ROI
// bool has_roi = false;
// Rect roi;
// if(has_roi==true)
// {
//   cam_model.setRegionOfInterest(roi);
//   cam_model.enableRegionOfInterest(has_roi);
// }


// if( has_roi )
// {
//   cv::Point dbg_tl = roi.tl(), dbg_br = roi.br();
//   dbg_tl.x -= 1; dbg_tl.y -= 1;
//   dbg_br.x += 1; dbg_br.y += 1;
//   cv::rectangle( drawed_img, dbg_tl, dbg_br, cv::Scalar(255,255,255));      
// }   


//if( has_roi )
//  src_img = src_img(roi);




  ///Cropping the input image 
  /*
  if(score > 0.70) 
  {
    Mat crop_img = src_img.clone();

    //Resizing the image according to the model 'box'
    Rect bb = dcm_p->getBB(match.id);
    Point2f  tlp = bb.tl(), brp = bb.br(); 
    tlp.x += match.img_offset.x - 30; tlp.y += match.img_offset.y - 30; 
    brp.x += match.img_offset.x + 30; brp.y += match.img_offset.y + 30;

    if(tlp.x > 0 && tlp.y >0 && brp.x < src_img.size().width && brp.y < src_img.size().height)
    {
      bb = Rect(tlp,brp);  
      Mat area_img = crop_img(bb);
      Mat edge = edge_map(bb);
      cv_ext::ImageStatisticsPtr img_stats_crop =
      cv_ext::ImageStatistics::createImageStatistics ( area_img, true );

      ImageTensorPtr dist_map_tensor_crop;
      dt.computeDistanceMapTensor(area_img, dist_map_tensor_crop, NUM_DIRECTION, DISTANCE_LAMBDA, smooth);

      dcm_p->setInput(dist_map_tensor_crop);

      vector<TemplateMatch> matches_crop;

      dcm_p->match(5, matches_crop, PIXEL_STEP, match.model);

      for( auto iter = matches_crop.begin(); iter!=matches_crop.end(); iter++)
      {
        if(next_image)
        {
          next_image=false;
          break;
        }

        TemplateMatch m = *iter;
        vector<float> n;
        vector<Point2f> pj;
        vector<Vec4f> pj_seg;
        double s;

        dcm_p->setOptimizerNumIterations(20);
        timer.reset();
        dcm_p->refinePosition(m.r_quat, m.t_vec);

        model_p->setModelView(m.r_quat, m.t_vec); 
        model_p->projectRasterPoints(pj, n, true); 
        model_p->projectRasterSegments(pj_seg, true); 

        s = evaluateScore( img_stats_crop, pj, n );

        std::cout << "CROPPED- Score: " << s << " /Distance: " << m.distance << " /Scoring time: " << timer.elapsedTimeMs() << 
        " - Model m" << m.model << " view " << m.id << endl;

        Mat area_draw;
        cvtColor ( edge,  area_draw, cv::COLOR_GRAY2BGR );
        cv_ext::drawSegments( area_draw, pj_seg, (s>0.70)? cv::Scalar ( 0,255,0 ) : cv::Scalar ( 0,0, 255 ),2 );
        showImage(area_draw,"Cropped match",true, 100);

        if(s>0.8)
        {
          //Moving the match according to the cropped region
          for(int i=0; i<(int) pj.size(); i++)
            pj[i] += tlp;

          for(int i=0; i<(int) pj_seg.size(); i++)
          {
            pj_seg[i][0] += tlp.x;
            pj_seg[i][1] += tlp.y;
            pj_seg[i][2] += tlp.x;
            pj_seg[i][3] += tlp.y;
          }

          cv_ext::drawSegments( drawed_img, pj_seg,  cv::Scalar ( 0,255,0 ), 2);
          putText(drawed_img, to_string(s), dcm_p->getBB(match.id).tl()+match.img_offset + Point2i(5,5), CV_FONT_HERSHEY_PLAIN, 3, cv::Scalar(255,0,0),2, 8);

          break;
        }
      }
    }
  }
*/


                 
//Old generate_models method 
//int n_z_step;
// if( delta_z != 0 && z_step != 0 )
// {
//   n_z_step = cvRound(2.0*delta_z/z_step);
//   z_step = 2.0*delta_z/double(n_z_step);
// }
// else
//   n_z_step = 0;

//DEBUG, media della luminosità
//Scalar lum = mean(src_img);
//cout << "Luminosità media: " << lum[0] << endl;
//END_DEBUG

/*DEBUG trasformations
double *quat;
quat = new double[4];
eigenQuat2Quat( match.r_quat, quat);
cout << "ROTAZIONE: " << quat[0] << "/" << quat[1] << "/" << quat[2] << "/" << quat[3] << "/"  << endl;
cout << " /TRASLATION: " << match.t_vec.x() << "/" << match.t_vec.y() << "/" << match.t_vec.z() << "/" << endl;
delete [] quat;
//DEBUG */