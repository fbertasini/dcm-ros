#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <signal.h>

#include "cv_ext/cv_ext.h"
#include <dcm_pack/ResultReady.h>
#include <boost/program_options.hpp>
#include "boost/filesystem.hpp"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

void handlerQuit(int s)
{
  printf("\nCaught signal %d\n",s);
  cout << "Exiting..." << endl;
  destroyAllWindows();
  exit(1); 
}

bool imageSaving(Mat img, int img_indx)
{
  string descriptor = "camera_image" + to_string(img_indx) + ".png";
  int result=imwrite(descriptor,img);
  if(!result)
  {
    cout << "Saving Error!" << endl; 
    return false;  
  }

  return true;
}

//Classe per gestire immagini ROS
class InputImage 
{
  Mat img;

  int model;
  double score;
  //Pixel position in the image
  Point2f position;
  Eigen::Quaterniond r_quat_;


  public:
    InputImage(){};

    void saveImage(Mat src_img){ img = src_img;}

    Mat getImage () {return img;}
    int getModel () {return model;}
    double getScore () {return score;}
    Point2f getPosition () {return position;}
    Eigen::Quaterniond getRotation () {return r_quat_;}

    void imageCallback(const sensor_msgs::ImageConstPtr& msg)
    {
      try
      {
        Mat src_img = cv_bridge::toCvShare(msg, "bgr8")->image;
        cout << "image subscribing..." << endl;
        img = src_img.clone();
      }
      catch (cv_bridge::Exception& e)
      {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
      }
    }

    bool newItem(dcm_pack::ResultReady::Request& req,
                  dcm_pack::ResultReady::Response& res)
    {
      model = req.model;
      score = req.score;
      position.x = req.position1;
      position.y = req.position2;

      Eigen::Quaterniond r_quat (req.rotation1, req.rotation2, req.rotation3, req.rotation4);
      r_quat_ = r_quat;
      res.res = 1;

      return true;
    }

};
      

int main(int argc, char** argv)
{
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = handlerQuit;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGQUIT, &sigIntHandler, NULL);

  ros::init(argc, argv, "image_results");

  InputImage input_img;
  ros::NodeHandle nh;
  startWindowThread();
  image_transport::ImageTransport it(nh);
  image_transport::Subscriber sub = it.subscribe("result/image", 3, &InputImage::imageCallback, &input_img);
  ros::ServiceServer service = nh.advertiseService("result_ready", &InputImage::newItem, &input_img);
  ros::Rate loop_rate(5);

  Mat src_img;

  cout << "Subscriber working ..." << endl;

  int rcv_img = 1 ;
  
  while(!sub.getNumPublishers())
  {}

  Mat old_img;
  
  while(sub.getNumPublishers())
  {
    ros::spinOnce();
    loop_rate.sleep();
    
    src_img = input_img.getImage();
  
    if( src_img.empty())
      continue;
    
    //Controlla se le immagini sono uguali
    auto diff = (src_img != old_img);
    if(countNonZero(diff) == 0)
      continue;
    
    old_img = src_img.clone();

    double score = input_img.getScore();
    cout << "Model m" << input_img.getModel() << " in position: (" << input_img.getPosition() <<")" << " with score: " << score << endl;

    rcv_img++;
    namedWindow("result_image");
    imshow("result_image", src_img);

  }
  

  cout << "Quitting..." << endl;
  destroyAllWindows();
  return 0;

}

