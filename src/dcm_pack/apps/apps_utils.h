#pragma once

#include "cv_ext/cv_ext.h"

#include <iostream>

#include <fstream>
#include <algorithm>
#include <opencv2/rgbd.hpp>

#include <boost/filesystem.hpp>
#include <boost/iterator/iterator_concepts.hpp>
#include <boost/tokenizer.hpp>
#include <boost/range/iterator_range.hpp>

//Per ROS
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <dcm_pack/ImageReady.h>
#include <dcm_pack/ResultReady.h>

using namespace std;
using namespace cv;

class InputImage 
{
  Mat img;
  string current_file;
  int i;
  bool new_image_req;

  public:
    InputImage();
    void saveImage (Mat src_img) { img = src_img;}
    Mat loadImage () {return img;}
    string loadName () {return current_file;}
    void imageRequest (bool state) {new_image_req = state;}
    void imageCallback(const sensor_msgs::ImageConstPtr& msg);
    bool newItem(dcm_pack::ImageReady::Request& req, dcm_pack::ImageReady::Response& res);
};

void objectPoseControlsHelp();
void parseObjectPoseControls ( int key ,cv::Mat &r_vec, cv::Mat &t_vec,
                               double r_inc = 0.1, double t_inc = 0.005 );

double evaluateScore ( cv_ext::ImageStatisticsPtr &img_stats_p,
                       const std::vector<cv::Point2f> &raster_pts,
                       const std::vector<float> &normal_directions );
bool userQuestion(std::string message);

bool readFileNamesFromFolder( const std::string& input_folder_name, std::vector< std::string >& names );

void imgSaturation (cv::Mat& src_img, double contrast = 1, int brightness = 0);