#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <dcm_pack/ImageReady.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <signal.h>

#include "cv_ext/cv_ext.h"
#include <boost/program_options.hpp>
#include "boost/filesystem.hpp"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

//Global Variable
VideoCapture cam;
int cam_indx;

void handlerQuit(int s)
{
  printf("\nCaught signal %d\n",s);
  cout << "Exiting..." << endl;
  if(cam.isOpened())
    cam.release();
  exit(1); 
}


Mat takeFrame()
{
  Mat frame;

  while(!cam.isOpened())
  {
    cout << "Failed to connect to the camera" << endl;
    cam.open(cam_indx);
  }
  cam>>frame;
  return frame;
}

bool imageSaving(Mat img, int img_indx)
{
  string descriptor = "camera_image" + to_string(img_indx) + ".png";
  int result=imwrite(descriptor,img);
  if(!result)
  {
    cout << "Saving Error!" << endl; 
    return false;  
  }

  return true;
}        

int main(int argc, char** argv)
{
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = handlerQuit;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGQUIT, &sigIntHandler, NULL);

  ros::init(argc, argv, "image_publisher");
  
  if(argc<3)
  {
    cout << "Input parameters missing. Insert the image folder and the camera." << endl;
    return 0;
  }

  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  ros::ServiceClient client = nh.serviceClient<dcm_pack::ImageReady>("image_ready");
  dcm_pack::ImageReady srv;
  image_transport::Publisher pub = it.advertise("initial/image", 3);
  ros::Rate loop_rate(5);

  cout << "Publisher working ..." << endl;

  cout << "Do you want load image from current folder [1] or from the camera [2] ?" << endl;
  int key;
  while(true)
  {
    cin >> key;
    if(key != 1 && key != 2)
      cout << "Wrong selection, retry." << endl;
    else
      break;
  }
  
  int snd_img = 0;

  if(key==1)
  {
    ///Image reading from folder
    path images_path(argv[1]);
    directory_iterator end_itr;
    directory_iterator itr(images_path);

    for(;itr!=end_itr;itr++)
    {
      if ( !is_regular_file(itr->path()))
            continue;

      string current_file = itr->path().string();

      Mat src_img = imread ( current_file, cv::IMREAD_COLOR );
      if( src_img.empty())
        continue;

      if( src_img.channels() == 1 )
        cvtColor ( src_img,  src_img, cv::COLOR_GRAY2BGR );

      srv.request.req = current_file ;
      sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", src_img).toImageMsg();
      while(!client.exists())
      {}

      pub.publish(msg);

      //Wait the service call
      while(client.call(srv)) 
      {
        if(srv.response.res==snd_img)
          continue;

        cout<< "Sent image" << snd_img + 1 << endl;
        snd_img++;
        break;
      }
    }

    srv.request.req = "end";
    client.call(srv);
  }
  else
  {
    string cam_i (argv[2]);
    cam_indx = stoi(cam_i);

    cam.open(cam_indx);

    while(!client.exists())
    {}

    while(client.exists())
    {
      Mat src_img = takeFrame();

      if( src_img.empty())
        continue;

      if( src_img.channels() == 1 )
        cvtColor ( src_img,  src_img, cv::COLOR_GRAY2BGR );

      string current_file = "camera_image_" + to_string(snd_img);

      srv.request.req = current_file ;
      //Funzione per salvare le immagini
      //imageSaving(src_img, snd_img);
      //snd_img++;
      //sleep(3);

      sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", src_img).toImageMsg();
      pub.publish(msg);

      if(client.call(srv)) 
      {

        ros::spinOnce();

        cout<< "Sent image" << snd_img + 1<< endl;

        snd_img++;
        loop_rate.sleep();

      }
    }
    
  }

  cout << "Quitting..." << endl;
  cam.release();
  return 0;

}

