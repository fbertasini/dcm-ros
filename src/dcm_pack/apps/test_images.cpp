#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
#include "boost/filesystem.hpp"
#include <cstdio>
#include <sstream>
#include <algorithm>
#include <map>
#include <opencv2/rgbd.hpp>
#include <signal.h>

#define CONTRAST 3

namespace po = boost::program_options;
namespace filesystem = boost::filesystem;
using namespace std;
using namespace cv;

struct AppOptions
{
  string images_name;
  int contrast;
};

void parseCommandLine( int argc, char **argv, AppOptions &options )
{
  string app_name( argv[0] );
  
  options.contrast = CONTRAST;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "images_directory,i", po::value<string > ( &options.images_name)->required(),
  "input images directory" );
 ;

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      std::cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;                
      exit(EXIT_SUCCESS);
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    std::cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    std::cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }
}

//TODO