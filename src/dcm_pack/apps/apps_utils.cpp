#include "apps_utils.h"

using namespace std;
using namespace cv;
using namespace boost::filesystem;

static const double OUTER_POINT_SCORE = 0.6;

//Class to handle ROS variables
InputImage::InputImage()
{
  i=0; 
  new_image_req = true;
}

void InputImage::imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    Mat src_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    img = src_img.clone();
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  }
}

bool InputImage::newItem(dcm_pack::ImageReady::Request& req, dcm_pack::ImageReady::Response& res)
{
  if(!new_image_req)
  {
    res.res = i;
    new_image_req = true;
    return false;
  }

  current_file = req.req;

  if(current_file.empty())
    return false;
  i++;
  res.res= i;

  return true;
}

void objectPoseControlsHelp()
{
  std::cout << "Object pose controls"<< std::endl;
  std::cout << "[j-l-k-i-u-o] translate the model along the X-Y-Z axis " << std::endl;
  std::cout << "[a-s-q-e-z-w] handle the rotation through axis-angle notation " << std::endl;  
}

void parseObjectPoseControls ( int key ,cv::Mat &r_vec, cv::Mat &t_vec,
                               double r_inc, double t_inc )
{
  switch( key )
  {
    case 'a': //Y rotate
    case 'A':
      r_vec.at<double>(1,0) += r_inc;
      break;
    case 's':
    case 'S':    
      r_vec.at<double>(1,0) -= r_inc;
      break;
    case 'w': //X rotate
    case 'W':
      r_vec.at<double>(0,0) += r_inc;
      break;
    case 'z':
    case 'Z':
      r_vec.at<double>(0,0) -= r_inc;
      break;
    case 'q': //Z rotate
    case 'Q':
      r_vec.at<double>(2,0) += r_inc;
      break;
    case 'e':
    case 'E':
      r_vec.at<double>(2,0) -= r_inc;
      break;
    case 'i': //Y translate
    case 'I':
      t_vec.at<double>(1,0) -= t_inc;
      break;
    case 'm':
    case 'M':
      t_vec.at<double>(1,0) += t_inc;
      break;
    case 'j': //X translate
    case 'J':
      t_vec.at<double>(0,0) -= t_inc;
      break;
    case 'l':
    case 'L':
      t_vec.at<double>(0,0) += t_inc;
      break;
    case 'u': //Z traslate
    case 'U':
      t_vec.at<double>(2,0) -= t_inc;
      break;
    case 'o':
    case 'O':
      t_vec.at<double>(2,0) += t_inc;
      break;
    default:
      break;
  }
}

double evaluateScore ( cv_ext::ImageStatisticsPtr &img_stats_p,
                       const std::vector<cv::Point2f> &raster_pts,
                       const std::vector<float> &normal_directions )
{
  boost::shared_ptr< std::vector<float> > g_dir_p;
  boost::shared_ptr< std::vector<float> > g_mag_p;

  g_dir_p =  img_stats_p->getGradientDirections ( raster_pts);
  g_mag_p =  img_stats_p->getGradientMagnitudes ( raster_pts);
 
  //DEBUG
  //cv_ext::showImage(img_stats_p->getGradientDirectionsImage(), "getGradientDirections", true, 10);
  //cv_ext::showImage(img_stats_p->getGradientMagnitudesImage(), "getGradientMagnitudes", true, 0);

  std::vector<float> &g_dir = *g_dir_p;
  std::vector<float> &g_mag = *g_mag_p;

  if ( !g_dir.size() || !g_mag_p->size() )
    return 0;

  double score = 0;
  for ( int i = 0; i < int(g_dir.size()); i++ )
  {
    float &direction = g_dir[i], magnitude = g_mag[i];

    //If the point is outside of image
    if ( img_stats_p->outOfImage ( direction ) )
      score += OUTER_POINT_SCORE;
    else //if(magnitude > 0.01)
      score +=  ((magnitude > 0.01)?1.0:magnitude ) * abs ( cos ( double ( direction ) - normal_directions[i] ) ); //DEBUG ((magnitude > 0.01)?1.0:magnitude ) *

  }

  return score/raster_pts.size();
}

bool userQuestion(std::string message)
{
  char key;
  while(true)
  {
    std::cout << "\n" << message << std::endl;
    std::cout << "[y-yes / n-no]" << std::endl;
    std::cin >> key;
    
    if(key == 'y' || key == 'Y')
      return true;
    if(key == 'n' || key == 'N')
      return false;
    std::cout << "Invalid key!" << std::endl;
  }
}

bool readFileNamesFromFolder( const string& input_folder_name, vector< string >& names )
{
  names.clear();
  if (!input_folder_name.empty())
  {
    path p(input_folder_name);
    for(auto& entry :  boost::make_iterator_range(directory_iterator(p), {}))
      names.push_back(entry.path().string());
    std::sort(names.begin(), names.end());
    return true;
  }
  else
  {
    return false;
  }
}

//For changing contrast or brightness of an image
void imgSaturation (Mat& src_img, double contrast, int brightness)
{
  Mat new_image = Mat::zeros(src_img.size(), src_img.type());

  for( int y = 0; y < src_img.rows; y++ ) 
  {
      for( int x = 0; x < src_img.cols; x++ )
    {
          for( int c = 0; c < src_img.channels(); c++ ) 
          {
            src_img.at<Vec3b>(y,x)[c] =
            saturate_cast<uchar>( contrast*src_img.at<Vec3b>(y,x)[c] + brightness );
          }
      }
  }
}
