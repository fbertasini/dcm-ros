#include <iostream>
#include <fstream>
#include <string>
#include <boost/program_options.hpp>
#include "boost/filesystem.hpp"
#include <cstdio>
#include <sstream>
#include <algorithm>
#include <map>
#include <opencv2/rgbd.hpp>
#include <signal.h>


/*For ROS
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <dcm_pack/ImageReady.h>
#include <dcm_pack/ResultReady.h>
*/

#include "apps_utils.h"
#include "cv_ext/cv_ext.h"
#include "raster_object_model3D.h"
#include "chamfer_matching.h"

#define NUM_DIRECTION 60
#define STEP_METERS 0.003 
#define CANNY_THRESHOLD 50 
#define CANNY_RATEO 5
#define LSD_PYR_LEVEL 6 
#define DISTANCE_LAMBDA 20 
#define PIXEL_STEP 8 
#define OUT_SCORE 0.6
#define PARALLELISM true 
#define SPACING_SIZE 10
#define CONTRAST 3
#define INSIDER_GAMMA 100

namespace po = boost::program_options;
namespace filesystem = boost::filesystem;
using namespace std;
using namespace cv;
using namespace cv_ext;

bool next_image = false;

void handlerQuit(int s)
{
  printf("\nCaught signal %d\n",s);
  cout << "Exiting..." << endl;
  destroyAllWindows();
  exit(1); 
}

void handlerStop(int s)
{
  printf("\nCaught signal %d\n",s);
  cout << "Next image..." << endl;
  next_image = true; 
}

struct AppOptions
{
  string models, camera_filename, images_directory, edge_type;
  int LSD_level, canny_rateo, canny_threshold, brightness;
  double contrast;
  int view_debug;
};

void parseCommandLine( int argc, char **argv, AppOptions &options )
{
  string app_name( argv[0] );

  options.edge_type = "CANNY"; 
  options.LSD_level = LSD_PYR_LEVEL;
  options.canny_rateo = CANNY_RATEO;
  options.canny_threshold = CANNY_THRESHOLD;
  options.contrast = CONTRAST;
  options.brightness = 0;
  options.view_debug = -1;
  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "model_filename_dir,m", po::value<string > ( &options.models)->required(),
  "STL, PLY, OBJ, ...  models file directory or the single model" )  
  ( "camera_filename,c", po::value<string > ( &options.camera_filename )->required(),
  "A YAML file that stores all the camera parameters (see the PinholeCameraModel object)" )
  ( "images_directory,i", po::value<string > ( &options.images_directory)->required(),
  "input images directory" )
  ( "edge_type,e", po::value<string > ( &options.edge_type),
  "type of edge detection, [CANNY]/[LSD]" )
  ( "LSD_pyramid_level,l", po::value<int > ( &options.LSD_level),
  "LSD pyramid level" )
  ( "Canny_rateo,r", po::value<int > ( &options.canny_rateo),
  "Canny rateo" )
  ( "Canny_threshold,t", po::value<int > ( &options.canny_threshold),
  "Canny threshold" )
  ( "Image_contrast,a", po::value<double > ( &options.contrast),
  "Contrast level of the images" )
  ( "image_brightness,b", po::value<int > ( &options.brightness),
  "Brightness level of the images" )
  ( "view_debug,d", po::value<int > ( &options.view_debug),
  "Number of the view to show" );
 ;

  po::variables_map vm;

  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;                
      exit(EXIT_SUCCESS);
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }
}


int main(int argc, char **argv)
{
  //Signal initializing
  struct sigaction quitHandler;
  struct sigaction stopHandler;

  quitHandler.sa_handler = handlerQuit;
  stopHandler.sa_handler = handlerStop;
  sigemptyset(&quitHandler.sa_mask);
  sigemptyset(&stopHandler.sa_mask);
  quitHandler.sa_flags = 0;
  stopHandler.sa_flags = 0;

  sigaction(SIGQUIT, &quitHandler, NULL);
  sigaction(SIGTSTP, &stopHandler, NULL);

  ///ROS initializing
  /*
  ros::init(argc, argv, "object_detection");
  ros::NodeHandle nh;

  InputImage input_img;
  image_transport::ImageTransport it(nh);

  dcm_pack::ResultReady srv;
  image_transport::Subscriber sub = it.subscribe("initial/image", 3, &InputImage::imageCallback, &input_img);
  image_transport::Publisher pub = it.advertise("result/image", 3);

  ros::ServiceServer service = nh.advertiseService("image_ready", &InputImage::newItem, &input_img);
  ros::ServiceClient client = nh.serviceClient<dcm_pack::ResultReady>("result_ready");

  ros::Rate loop_rate(5);
  */

  AppOptions options;
  parseCommandLine( argc, argv, options );
  
  ///Time for testing 
  cv_ext::BasicTimer timer;

  ///Cam_model reading
  cv_ext::PinholeCameraModel cam_model;
  cam_model.readFromFile(options.camera_filename);
  int img_w = cam_model.imgWidth(), img_h = cam_model.imgHeight();

  cout << "Loading models from directory : "<<options.models<< endl;
  cout << "Loading camera parameters from file : "<<options.camera_filename<< endl;
  cout << "Loading input images directory : "<<options.images_directory<< endl;
  cout << "Using edge detector type : "<<options.edge_type<< endl;

  ///Model reading, could be a single model or a directory
  map< int, RasterObjectModel3DPtr > obj_model;
  int pos = options.models.length() - 4;
  string ext = options.models.substr(pos);    //File extension
  int model_num = 0;

  timer.reset();
  if(!ext.compare(".stl") || !ext.compare(".ply") || !ext.compare(".obj"))
  {
    pos -= 1;
    int num = stoi(options.models.substr(pos,1)); //Model number

    RasterObjectModel3DPtr obj_p (new RasterObjectModel3D () );
    RasterObjectModel3D &obj_m = *obj_p;

    obj_m.setCamModel( cam_model );
    obj_m.setStepMeters ( STEP_METERS ); 
    obj_m.setUnitOfMeasure(RasterObjectModel::MILLIMETER);
    if ( !obj_m.setModelFile ( options.models ) )
      return -1;
    obj_m.computeRaster(); 

    obj_model.insert (pair<int,RasterObjectModel3DPtr>(num,obj_p));
    options.models = options.models.substr(0, pos-1); //Precomputed_model directory

    model_num++;
  }
  else  //Up to 9 model!
  {
    filesystem::path models_path (options.models);
    filesystem::directory_iterator m_end_itr;
    
    for (filesystem::directory_iterator itr(models_path); itr != m_end_itr; ++itr)
    {
      if ( !filesystem::is_regular_file(itr->path()))
        continue;

      string current_file = itr->path().string();
      pos = current_file.length() - 5;
      int num = stoi(current_file.substr(pos,1)); //Model number

      RasterObjectModel3DPtr obj_p (new RasterObjectModel3D () );
      RasterObjectModel3D &obj_m = *obj_p;

      obj_m.setCamModel( cam_model );
      obj_m.setStepMeters ( STEP_METERS ); 
      obj_m.setUnitOfMeasure(RasterObjectModel::MILLIMETER);
      if ( !obj_m.setModelFile ( current_file ) )
        return -1;
      obj_m.computeRaster(); 

      obj_model.insert (pair<int,RasterObjectModel3DPtr>(num,obj_p));
      model_num++;
    }
  }
  cout << "Model Loading time: " << timer.elapsedTimeMs() << endl;
  
  ///Views reading
  filesystem::path pre_models_path (options.models + "Pre_Models/"); //For precomputed_model directory
  filesystem::directory_iterator m_end_itr;

  timer.reset();
  for(filesystem::directory_iterator itr(pre_models_path); itr != m_end_itr; ++itr)
  {
    if ( !filesystem::is_regular_file(itr->path()))
      continue;

    string current_file = itr->path().string();
    int number_pos = current_file.length() - 5;
    int num = stoi(current_file.substr(number_pos,1));  //Model number

    auto obj_p = obj_model.find(num);
    if(obj_p != obj_model.end())
    {
      cout << "Loading precomputed views " << num << endl;
      (obj_p->second)->loadPrecomputedModelsViews(current_file); 
    }
  } 
  cout << "Views Loading time: " << timer.elapsedTimeMs() << endl;

  ///Distance trasform initialization
  DistanceTransform dt;
  //dt.setDistThreshold(30);
  dt.enableParallelism(PARALLELISM);
  
  
  ///Edge detection initialization
  if( !options.edge_type.compare("LSD") )
  {
    LSDEdgeDetectorUniquePtr edge_detector_ptr( new LSDEdgeDetector());
    edge_detector_ptr->setPyrNumLevels(options.LSD_level); 
    dt.setEdgeDetector(move(edge_detector_ptr));
  }
  else if( !options.edge_type.compare("CANNY") )
  {
    CannyEdgeDetectorUniquePtr edge_detector_ptr( new CannyEdgeDetector() );
    edge_detector_ptr->setLowThreshold(options.canny_threshold);
    edge_detector_ptr->setRatio(options.canny_rateo); 
    edge_detector_ptr->enableRGBmodality(true);    
    dt.setEdgeDetector(move(edge_detector_ptr));
  }

  ///Directional Chamfer Matching initialization
  multimap <int,DirectionalChamferMatchingPtr> dcm;

  timer.reset();
  #pragma omp parallel for schedule(dynamic)
  for(int i=1; i<=model_num; i++)
  {
    if(obj_model.find(i)==obj_model.end())
      continue;

    DirectionalChamferMatchingPtr dcm_p (new DirectionalChamferMatching()); 
    dcm_p->setTemplateModel(obj_model[i]); 
    dcm_p->setNumDirections(NUM_DIRECTION);
    dcm_p->enableParallelism(PARALLELISM);
    dcm_p->enableVerbouseMode(false);
    dcm_p->setupExhaustiveMatching(SPACING_SIZE);

    dcm.insert(pair<int,DirectionalChamferMatchingPtr>(i,dcm_p));
  }
  cout << "DCM initialization time: " << timer.elapsedTimeMs() << endl;


  bool smooth = false;
  bool fromROS =  false;
  filesystem::path images_path(options.images_directory);
  filesystem::directory_iterator end_itr;
  filesystem::directory_iterator itr(images_path);

  namedWindow("original image", CV_WINDOW_NORMAL);
  namedWindow("Best scores", CV_WINDOW_NORMAL);
  resizeWindow("original image", cam_model.imgWidth()*0.5, cam_model.imgHeight()*0.5);
  resizeWindow("Best scores", cam_model.imgWidth()*0.5, cam_model.imgHeight()*0.5);
  
  /* //FOR ROS
  if(userQuestion("Would you take images from folders ? (Alternatively the images will be taken from ROS Topic)"))
      fromROS = false;
  else
      fromROS = true;
  */

  while(true)
  {
    string current_file;
    Mat src_img;
    Mat resized_img;

    if(fromROS)
    {
      /*
      ros::spinOnce();
      loop_rate.sleep();
      src_img = input_img.loadImage();
      current_file = input_img.loadName();

      if(current_file == "end")
        break;
    
      if( src_img.empty())
        continue;
        */
    } 
    else
    {
      if(itr!=end_itr)
      {
        if (!filesystem::is_regular_file(itr->path()))
        {
          itr++;
          continue;
        }

        current_file = itr->path().string();
        src_img = imread ( current_file, cv::IMREAD_COLOR );
        itr++;

        if( src_img.empty())
        continue;       
      }
      else
        break;
    }

    //Image selection
    if(userQuestion("Would you test the image: " + current_file + "?") == false)
      continue;

    resize(src_img , resized_img , cv::Size(img_w, img_h));
    if( resized_img.channels() == 1 )
      cvtColor ( resized_img,  src_img, cv::COLOR_GRAY2BGR );
    else
      src_img = resized_img;

    //Contrast modification
    imgSaturation(src_img, options.contrast, options.brightness);
    showImage(src_img,"original image", true, 1);

    //Image statistics for scoring function
    cv_ext::ImageStatisticsPtr img_stats_p =
    cv_ext::ImageStatistics::createImageStatistics ( src_img, true );

    ///Distance Trasform tensor generating
    ImageTensorPtr dist_map_tensor_ptr;
    dt.computeDistanceMapTensor(src_img, dist_map_tensor_ptr, NUM_DIRECTION, DISTANCE_LAMBDA, smooth);
    Mat edge_map;
    dt.getEdgeMap(edge_map);
    /*
    namedWindow("Edge image", CV_WINDOW_NORMAL);
    resizeWindow("Edge image", edge_map.cols*0.5, edge_map.rows*0.5);
    showImage(edge_map,"Edge image", true, 1);
    */

    Mat drawed_img = edge_map.clone();
    if( drawed_img.channels() == 1 )
      cvtColor ( drawed_img,  drawed_img, cv::COLOR_GRAY2BGR );
    cv::Point p(20,20);
    putText(drawed_img, current_file, p , CV_FONT_HERSHEY_PLAIN, 2, cv::Scalar(255,0,0), 2, 8);

    //Inserting the tensor in each direct chamfer matching pointer
    for(auto iter = dcm.begin(); iter!=dcm.end(); iter++)
      iter->second->setInput(dist_map_tensor_ptr);

    ///Matching operations
    multimap<double, TemplateMatch> matches;

    for(auto iter = dcm.begin(); iter!=dcm.end(); iter++)
    {
      timer.reset();
      iter->second->matchAltFast(matches, PIXEL_STEP, iter->first, edge_map, INSIDER_GAMMA, options.view_debug);
      cout << "Matching time for model " << iter->first << " : " << timer.elapsedTimeMs() << endl;
    }

    //For multi object debug
    map<int,int> model_found;
    
    //Score evaluation
    for( auto iter = matches.begin(); iter != matches.end(); iter++) 
    {
      if(next_image)
      {
        next_image=false;
        break;
      }
      
      TemplateMatch match = iter->second;

      //DEBUG for multi image
      if(match.model == model_found.find(match.model)->second)
        continue;

      vector<Point2f> proj_pts;
      vector<Vec4f> proj_seg;
      vector<float> normals;
      RasterObjectModel3DPtr model_p =  obj_model.find(match.model)->second;
      DirectionalChamferMatchingPtr dcm_p = dcm.find(match.model)->second;
      
      dcm_p->setOptimizerNumIterations(15);//Mean square iterations number 
      timer.reset();
      dcm_p->refinePosition( match.r_quat, match.t_vec);
      
      model_p->setModelView( match.r_quat, match.t_vec); 
      model_p->projectRasterPoints(proj_pts, normals, true);
      model_p->projectRasterSegments(proj_seg, true); 

      //Reducing the point cloud where is too crowded
      vector<Point2f> proj_pts_reduc;
      vector<float> normals_reduc;
      dcm_p->pointsReduction(proj_pts, normals, proj_pts_reduc, normals_reduc, SPACING_SIZE);

      // DEBUG single match 
      if(match.id == options.view_debug)
      {
        Mat img_deb(img_h, img_w, CV_8UC3, cv::Scalar(0, 0, 0));
        cv_ext::drawPoints(img_deb, proj_pts_reduc);
        namedWindow("debug", CV_WINDOW_NORMAL);
        resizeWindow("debug", img_deb.cols*0.5, img_deb.rows*0.5);
        showImage(img_deb, "debug", true, 10);
      }
  
      double score = evaluateScore( img_stats_p, proj_pts_reduc, normals_reduc );
 
      cout << "Distance: " << match.distance << " /Score: " << score << " /Scoring time: " << timer.elapsedTimeMs() << 
      "ms " << " - Model m" << match.model << " - view " << match.id <<  " /Position: ("<< match.img_offset.x << ", " << match.img_offset.y << ")" << endl; //" /Model points: " << proj_pts_reduc.size() << endl;

      cv_ext::drawSegments(drawed_img, proj_seg, (score>0.75)? cv::Scalar ( 0,255,0 ): cv::Scalar ( 0,0,255 ),3);
      if(score>0.75) 
        putText(drawed_img, to_string(score), dcm_p->getBB(match.id).tl()+match.img_offset + Point2i(-15,-15), CV_FONT_HERSHEY_PLAIN, 3, cv::Scalar(255,0,0),3, 8);

      showImage(drawed_img,"Best scores",true, 10); 


      if(score > 0.75)
      {
        Mat mask = model_p->getMask();
        Mat inside_edge = ~edge_map.clone();
        cv_ext::drawSegments( mask, proj_seg, cv::Scalar ( 255,255,255),10);
        inside_edge.setTo(cv::Scalar(0,0,0), mask);

        cout << "Inner edge points: " << countNonZero(inside_edge) << endl;
        cvtColor ( inside_edge,  inside_edge, cv::COLOR_GRAY2BGR );

        cv_ext::drawSegments( inside_edge, proj_seg, cv::Scalar ( 0,255,0),10);
        model_found.insert(pair<int,int>(match.model, match.model));

        namedWindow("Match & Mask", CV_WINDOW_NORMAL);
        resizeWindow("Match & Mask", cam_model.imgWidth()*0.5, cam_model.imgHeight()*0.5);
        showImage(inside_edge,"Match & Mask",true, 10);

        if(!userQuestion("Object found; continue to view the matches anyway?"))
        {
          next_image = false;
          model_found.clear();
          break;
        }
      }

    }
    matches.clear();
    
  }

  destroyAllWindows();
  /*
  sub.shutdown();
  pub.shutdown();
  service.shutdown();
  client.shutdown();
  nh.shutdown();
  */

  cout << "Matching process terminated" << endl;

  waitKey();

  return 0;
}