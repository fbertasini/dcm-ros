#include <cstdio>
#include <string>
#include <sstream>
#include <boost/program_options.hpp>
#include <signal.h>

#include "cv_ext/cv_ext.h"
#include "raster_object_model3D.h"
#include "raster_object_model2D.h"

#include "apps_utils.h"

#define STEP_METERS 0.003  
#define SEGMENT_ENABLE false 
#define DEFAULT_YAW_STEP 10
#define DEFAULT_N_Z_STEP 1 

namespace po = boost::program_options;
using namespace std;
using namespace cv;

void handlerQuit(int s)
{
  printf("\nCaught signal %d\n",s);
  cout << "Exiting..." << endl;
  destroyAllWindows();
  exit(1); 
}

void quickGuide()
{
  cout << "Use the keyboard to move the object model:" << endl<< endl;
  objectPoseControlsHelp();
}

struct AppOptions
{
 string model_filename, pre_models_filename, 
        camera_filename, image_filename;
  int yaw_step, n_z_step;
};

void parseCommandLine( int argc, char **argv, AppOptions &option )
{
  string app_name( argv[0] );
  option.yaw_step = DEFAULT_YAW_STEP;
  option.n_z_step = DEFAULT_N_Z_STEP;

  po::options_description desc ( "OPTIONS" );
  desc.add_options()
  ( "help,h", "Print this help messages" )
  ( "model_filename,m", po::value<string > ( &option.model_filename )->required(),
    "STL, PLY, OBJ, ...  model file" )
  ( "pre_models_filename,p", po::value<string > ( &option.pre_models_filename)->required(),
    "Input/output precomputed models file" )
  ( "camera_filename,c", po::value<string > ( &option.camera_filename )->required(),
    "A YAML file that stores all the camera parameters (see the PinholeCameraModel object)" )
  ( "image_filename,i", po::value<string > ( &option.image_filename ),
    "Optional background image file" )
  ( "yaw_step,y", po::value<int > ( &option.yaw_step ),
    "The rotation step for each icosphere vertices" )
  ( "n_z_step,n", po::value<int > ( &option.n_z_step ),
    "The number of scales generated" );
  
  po::variables_map vm;
  try
  {
    po::store ( po::parse_command_line ( argc, argv, desc ), vm );

    if ( vm.count ( "help" ) )
    {
      cout << "USAGE: "<<app_name<<" OPTIONS"
                << endl << endl<<desc;
      exit(EXIT_SUCCESS);
    }

    po::notify ( vm );
  }

  catch ( boost::program_options::required_option& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }

  catch ( boost::program_options::error& e )
  {
    cerr << "ERROR: " << e.what() << endl << endl;
    cout << "USAGE: "<<app_name<<" OPTIONS"
              << endl << endl<<desc;
    exit(EXIT_FAILURE);
  }

}

int main(int argc, char **argv)
{
  //Signal initialization
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = handlerQuit;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGQUIT, &sigIntHandler, NULL);
  sigaction(SIGINT, &sigIntHandler, NULL);
  sigaction(SIGTSTP, &sigIntHandler, NULL);

  ///Time for testing 
  cv_ext::BasicTimer timer;

  AppOptions options;
  parseCommandLine( argc, argv, options );

  //Parameters for the multi-views                                                                                                                                                
  double scale_factor = 1.0;
  int top_boundary = -1, bottom_boundary = -1, left_boundary = -1, rigth_boundary = -1;
  double delta_rp = -1, rp_step = 10,
         delta_yaw = -1, yaw_step = options.yaw_step, //was 10
         z_step = 0.025;
  //Number of different scales of the views, if 1 only the original scale is taken
  int    n_z_step = options.n_z_step; 
  //Number of refining iterations of the icosphere, if 0 the icosphere has 12 verices
  int    n_refine = 2; 
  


  //Initial reference system
  Mat r_vec = (Mat_<double>(3,1) << 0,0,0),
      t_vec = (Mat_<double>(3,1) << 0,0,0.25);

  //Camera parameters reading
  cv_ext::PinholeCameraModel cam_model;
  cam_model.readFromFile(options.camera_filename);
  cam_model.setSizeScaleFactor(scale_factor);
  int img_w = cam_model.imgWidth(), img_h = cam_model.imgHeight();
  
  bool has_roi = false;
  cv::Rect roi;
  
  //For ROI
  if( top_boundary != -1 || bottom_boundary != -1 || 
      left_boundary != -1 || rigth_boundary != -1 )
  {
    Point tl(0,0), br(img_w, img_h);
    
    if( top_boundary != -1 ) tl.y = top_boundary;
    if( left_boundary != -1 ) tl.x = left_boundary;
    if( bottom_boundary != -1 ) br.y = bottom_boundary;
    if( rigth_boundary != -1 ) br.x = rigth_boundary;
    
    has_roi = true;
    roi = cv::Rect(tl, br);
    cam_model.setRegionOfInterest(roi);
    cam_model.enableRegionOfInterest(true);
    roi = cam_model.regionOfInterest();
  }
   
  cout << "Loading model from file : "<<options.model_filename<< endl;
  cout << "Loading precomputed models from file : "<<options.pre_models_filename<< endl;  
  cout << "Loading camera parameters from file : "<<options.camera_filename<< endl;
  if( !options.image_filename.empty() )
    cout << "Loading background image from file : "<<options.image_filename<< endl;
  cout << "Scale factor : "<<scale_factor<< endl;
  if(has_roi)
    cout << "Region of interest : "<<roi<< endl;
  
  if( delta_rp > 90 ) delta_rp = 90;
  if( delta_yaw > 90 ) delta_yaw = 90;
  if( rp_step < 0 ) rp_step = 0;
  if( yaw_step < 0 ) yaw_step = 0;
  
  cout << "Roll/pitch tollerance : ";
  if( delta_rp < 0 )
    cout << "full dimensions";
  else
    cout << delta_rp;
  cout <<" sample step "<<rp_step<<" [degrees]"<< endl;
  cout << "Yaw tollerance : ";
  if( delta_yaw < 0 )
    cout <<"full dimension";
  else
    cout <<delta_yaw;
  cout <<" sample step "<<yaw_step<<" [degrees]"<< endl;
  cout << "Z tollerance : " << n_z_step << " times with sample step "<<z_step<<" [meters]"<< endl;

  //Convert to radiants
  delta_rp *= M_PI/180.0; rp_step *= M_PI/180.0;
  delta_yaw *= M_PI/180.0; yaw_step *= M_PI/180.0;
         
  //CAD model reading
  RasterObjectModel3D obj_model;
  obj_model.setCamModel( cam_model );
  obj_model.setStepMeters(STEP_METERS);
  obj_model.setUnitOfMeasure(RasterObjectModel::MILLIMETER);

  if(!obj_model.setModelFile( options.model_filename ))
    return -1;

  //obj_model.setMinSegmentsLen(0.01);
  obj_model.computeRaster();
  obj_model.loadPrecomputedModelsViews(options.pre_models_filename);
  
  //Background image reading
  cv::Mat background_img;
  if(!options.image_filename.empty())
  {
    background_img = cv::imread(options.image_filename, cv::IMREAD_COLOR);
    cv::resize(background_img, background_img, Size(img_w, img_h));
  }
  
  vector<Vec4f> raster_segs;
  vector<Point2f> raster_pts;

  bool view_mode = obj_model.numPrecomputedModelsViews() != 0, 
       draw_axis = false, store_model = false, store_multi_model = false, save_models = false;
  int cur_pre_model = 0;
  
  quickGuide();

  bool exit_now = false;
  bool delete_models = false;
  Point text_org_guide1 (20, 30), text_org_guide2(20, 65), text_org_model (20, 150),
        text_org_x(20, img_h - 90), text_org_y(20, img_h - 50), text_org_z(20, img_h - 10),
        text_org_a(img_w - 300, img_h - 90), text_org_b(img_w - 300, img_h - 50), text_org_c(img_w - 300, img_h - 10);
  Scalar text_color;
  
  while( !exit_now )
  { 
    stringstream sstr_guide1, sstr_guide2, sstr_model;

    if(view_mode)   //Display the saved views
    {
      sstr_guide1<<"Use [9] an [0] to switch the previuois and next models, [SPACE] to add more views.";
      sstr_guide2<<"[r] to show axis.";

      obj_model.setModelView(cur_pre_model);
      sstr_model<<"VIEW MODE - Model ";
      sstr_model<<cur_pre_model + 1;
      sstr_model<<" of ";
      sstr_model<<obj_model.numPrecomputedModelsViews();
      
      text_color = Scalar(0x44,0x62,0x35);
  
    }
    else    //Display the model
    {
      
      sstr_guide1<<"Use [1] to store the current model, [9] to store a set of views close to the current one, [0] to save the views.";
      sstr_guide2<<"Use [g] to delete models,  [SPACE] to show all stored models,  [r] to show axis";
      obj_model.setModelView(r_vec, t_vec);
      
      if( store_model )   //Store a single view
      {
        obj_model.storeModelView();
        store_model = false;
      }
      else if( store_multi_model )    //Store multi-views
      {
        cout << "Storing multiple model..." << endl;
        timer.reset();

        Eigen::Quaterniond ref_qr;
        Eigen::Vector3d ref_t;
        obj_model.modelView(ref_qr,ref_t);
        std::vector<cv::Point3d> cap_points;
        cv_ext::vector_Quaterniond quat_rotations;
        
        if( rp_step )
        {
          if( delta_rp < 0 )
            cv_ext::createIcosphere( cap_points, n_refine , 1.0 ); //Icosphere generation with 12 vertices 
          else
            cv_ext::createIcospherePolarCapFromAngle( cap_points, n_refine, M_PI/2 - delta_rp, 1.0, true );
          
          cout<<cap_points.size()<< " icospere's vertices."<<endl;
          if( cap_points.size() )
          {
            if( delta_yaw != 0 && yaw_step != 0 )
            {
              //Rotating the model around the 12 vertices
              if( delta_yaw <= 0 )
              {
                int num_rotations = round(2*M_PI/yaw_step);
                cv_ext::sampleRotationsAroundVectors( cap_points, quat_rotations, 
                                                      num_rotations , cv_ext::COORDINATE_Z_AXIS );
              }
              else
                cv_ext::sampleRotationsAroundVectors( cap_points, quat_rotations, 
                                                      -delta_yaw, delta_yaw, yaw_step,
                                                      cv_ext::COORDINATE_Z_AXIS );
            }
            else
              cv_ext::sampleRotationsAroundVectors( cap_points, quat_rotations, 1, cv_ext::COORDINATE_Z_AXIS );
          
            cout<<quat_rotations.size()<< " rotation position."<<endl;
          Eigen::Isometry3d cur_tr, ref_tr, final_tr; //Represents an homogeneous transformation in a 3d space
            
            //Creating different model scale
            for(int i_z = 0; i_z < n_z_step; i_z++, ref_t(2)+= z_step) //Model closer every cicle
            {
              //Taking current position and traslation of the model
              ref_tr.fromPositionOrientationScale( ref_t, ref_qr, Eigen::Vector3d::Ones());
              for( auto &q : quat_rotations)
              {
                cur_tr.fromPositionOrientationScale( Eigen::Vector3d::Zero(), q, Eigen::Vector3d::Ones());
                final_tr = ref_tr*cur_tr;
                obj_model.setModelView( Eigen::Quaterniond(final_tr.rotation()), ref_t );
                obj_model.storeModelView();  
              }
              
            }
            cout<< "Operation done. " << (quat_rotations.size()*n_z_step) << " views."<< "Time spent: " << timer.elapsedTimeMs() << endl;
          }
        }
        store_multi_model = false;
      }

      sstr_model<<"EDIT MODE - Stored ";
      sstr_model<<obj_model.numPrecomputedModelsViews();
      sstr_model<<" models";
      
      if(save_models) //Save the stored views
      {
        cout << "Saving models..." << endl;
        obj_model.savePrecomputedModelsViews(options.pre_models_filename);
        save_models = false;
        sstr_model<<" (Saved)";
        cout << "Models saved." << endl;
      }

      if(delete_models)  //TODO
      {
        //TODO
        delete_models = false;
        //sstr_model<<" (Deleted)";
      }
      
      text_color = Scalar(0x1A,0x2A,0xAD);
    }
    
    Mat background, draw_img;
    if( background_img.empty() )
      background = Mat( Size(img_w,img_h),
                        DataType<Vec3b>::type, CV_RGB( 0,0,0));
    else
      background = background_img.clone();

    if( has_roi )
      draw_img = background(roi);
    else
      draw_img = background;
    
    obj_model.projectRasterPoints(raster_pts);
    cv_ext::drawPoints( draw_img, raster_pts );
    

    if( draw_axis )   //Display the 3 axes
    {
      vector< Vec4f > proj_segs_x, proj_segs_y,  proj_segs_z;
      obj_model.projectAxes ( proj_segs_x, proj_segs_y, proj_segs_z );

      cv_ext::drawSegments( draw_img, proj_segs_x, Scalar(0, 0, 255) );
      cv_ext::drawSegments( draw_img, proj_segs_y, Scalar(0, 255, 0) );
      cv_ext::drawSegments( draw_img, proj_segs_z, Scalar(255, 0, 0) );
    }
    
    if( has_roi )
    {
      cv::Point dbg_tl = roi.tl(), dbg_br = roi.br();
      dbg_tl.x -= 1; dbg_tl.y -= 1;
      dbg_br.x += 1; dbg_br.y += 1;
      cv::rectangle( background, dbg_tl, dbg_br, cv::Scalar(255,255,255));
    }
    
    putText(background, sstr_guide1.str(), text_org_guide1, CV_FONT_HERSHEY_PLAIN, 2, 
        text_color, 2, 8);
    putText(background, sstr_guide2.str(), text_org_guide2, CV_FONT_HERSHEY_PLAIN, 2, 
        text_color, 2, 8);
    putText(background, sstr_model.str(), text_org_model, CV_FONT_HERSHEY_SIMPLEX, 2, 
            text_color, 2, 8);

    Mat_<double> print_t_vec;
    Mat_<double> print_r_vec;
    obj_model.modelView(print_r_vec,print_t_vec);

    stringstream sstr_x, sstr_y, sstr_z;
    sstr_x<<"X:"; sstr_y<<"Y:"; sstr_z<<"Z:";
    sstr_x<<print_t_vec.at<double>(0); sstr_y<<print_t_vec.at<double>(1); sstr_z<<print_t_vec.at<double>(2);
    putText(background, sstr_x.str(), text_org_x, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    putText(background, sstr_y.str(), text_org_y, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    putText(background, sstr_z.str(), text_org_z, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    
    stringstream sstr_a, sstr_b, sstr_c;
    sstr_a<<"Alfa(x):"; sstr_b<<"Beta(y):"; sstr_c<<"Gamma(z):";

    sstr_a<<((abs(print_r_vec.at<double>(0,0))<1e-10)? 0:print_r_vec.at<double>(0,0)); 
    sstr_b<<((abs(print_r_vec.at<double>(1,0))<1e-10)? 0:print_r_vec.at<double>(1,0)); 
    sstr_c<<((abs(print_r_vec.at<double>(2,0))<1e-10)? 0:print_r_vec.at<double>(2,0)); 
    putText(background, sstr_a.str(), text_org_a, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    putText(background, sstr_b.str(), text_org_b, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    putText(background, sstr_c.str(), text_org_c, CV_FONT_HERSHEY_PLAIN, 2, text_color, 2, 8);
    
    namedWindow("Generate Models", CV_WINDOW_NORMAL);
    resizeWindow("Generate Models", cam_model.imgWidth()*0.5, cam_model.imgHeight()*0.5);
    imshow("Generate Models", background);
    int key = cv_ext::waitKeyboard();
    
    //Key cases
    if( !view_mode )
    {
      parseObjectPoseControls( key, r_vec, t_vec);
      switch(key)
      {
        case '1':
          store_model = true;
          break;
        case '9':
          store_multi_model = true;
          break;             
        case '0':
          save_models = true;
          break;
        case 'g':
          delete_models = true;
          break;  
      }      
    }
    else
    {
      switch(key)
      {      
        case '0':
          cur_pre_model++;
          if( cur_pre_model >= obj_model.numPrecomputedModelsViews() )
            cur_pre_model = 0;
          break;
        case '9':
          cur_pre_model--;
          if( cur_pre_model < 0 )
            cur_pre_model = obj_model.numPrecomputedModelsViews() - 1;
          break;
        // case 'c' :
        //   remove_model=true;
        //   break;
      }
    }
    
    switch(key)
    {
      case 'r':
        draw_axis = !draw_axis;
        break;
      case ' ':
        view_mode = !view_mode && obj_model.numPrecomputedModelsViews();
        break;        
      case cv_ext::KEY_ESCAPE:
        exit_now = true;
        destroyAllWindows();
        break;
    }
  }

  return 0;
}


