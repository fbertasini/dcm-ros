
"use strict";

let ResultReady = require('./ResultReady.js')
let ImageReady = require('./ImageReady.js')

module.exports = {
  ResultReady: ResultReady,
  ImageReady: ImageReady,
};
