(cl:in-package dcm_pack-srv)
(cl:export '(MODEL-VAL
          MODEL
          SCORE-VAL
          SCORE
          POSITION1-VAL
          POSITION1
          POSITION2-VAL
          POSITION2
          POSITION3-VAL
          POSITION3
          ROTATION1-VAL
          ROTATION1
          ROTATION2-VAL
          ROTATION2
          ROTATION3-VAL
          ROTATION3
          ROTATION4-VAL
          ROTATION4
          RES-VAL
          RES
))