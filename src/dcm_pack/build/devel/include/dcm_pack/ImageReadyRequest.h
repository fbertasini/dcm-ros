// Generated by gencpp from file dcm_pack/ImageReadyRequest.msg
// DO NOT EDIT!


#ifndef DCM_PACK_MESSAGE_IMAGEREADYREQUEST_H
#define DCM_PACK_MESSAGE_IMAGEREADYREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace dcm_pack
{
template <class ContainerAllocator>
struct ImageReadyRequest_
{
  typedef ImageReadyRequest_<ContainerAllocator> Type;

  ImageReadyRequest_()
    : req()  {
    }
  ImageReadyRequest_(const ContainerAllocator& _alloc)
    : req(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _req_type;
  _req_type req;





  typedef boost::shared_ptr< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> const> ConstPtr;

}; // struct ImageReadyRequest_

typedef ::dcm_pack::ImageReadyRequest_<std::allocator<void> > ImageReadyRequest;

typedef boost::shared_ptr< ::dcm_pack::ImageReadyRequest > ImageReadyRequestPtr;
typedef boost::shared_ptr< ::dcm_pack::ImageReadyRequest const> ImageReadyRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::dcm_pack::ImageReadyRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::dcm_pack::ImageReadyRequest_<ContainerAllocator1> & lhs, const ::dcm_pack::ImageReadyRequest_<ContainerAllocator2> & rhs)
{
  return lhs.req == rhs.req;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::dcm_pack::ImageReadyRequest_<ContainerAllocator1> & lhs, const ::dcm_pack::ImageReadyRequest_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace dcm_pack

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "b8dc53fbc3707f169aa5dbf7b19d2567";
  }

  static const char* value(const ::dcm_pack::ImageReadyRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xb8dc53fbc3707f16ULL;
  static const uint64_t static_value2 = 0x9aa5dbf7b19d2567ULL;
};

template<class ContainerAllocator>
struct DataType< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "dcm_pack/ImageReadyRequest";
  }

  static const char* value(const ::dcm_pack::ImageReadyRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "#request\n"
"string req\n"
;
  }

  static const char* value(const ::dcm_pack::ImageReadyRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.req);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ImageReadyRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::dcm_pack::ImageReadyRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::dcm_pack::ImageReadyRequest_<ContainerAllocator>& v)
  {
    s << indent << "req: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.req);
  }
};

} // namespace message_operations
} // namespace ros

#endif // DCM_PACK_MESSAGE_IMAGEREADYREQUEST_H
