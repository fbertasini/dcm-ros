#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/federico/ROS/image_transport_ws/src/dcm_pack/build/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/federico/ROS/image_transport_ws/src/dcm_pack/build/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/federico/ROS/image_transport_ws/src/dcm_pack/build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/federico/ROS/image_transport_ws/src/dcm_pack/build'
export PYTHONPATH="/home/federico/ROS/image_transport_ws/src/dcm_pack/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/federico/ROS/image_transport_ws/src/dcm_pack/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/federico/ROS/image_transport_ws/src/dcm_pack:$ROS_PACKAGE_PATH"