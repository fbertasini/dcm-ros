# CMake generated Testfile for 
# Source directory: /home/federico/ROS/image_transport_ws/src
# Build directory: /home/federico/ROS/image_transport_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("dcm_pack")
