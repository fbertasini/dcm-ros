// Generated by gencpp from file dcm_pack/LocalizePartRequest.msg
// DO NOT EDIT!


#ifndef DCM_PACK_MESSAGE_LOCALIZEPARTREQUEST_H
#define DCM_PACK_MESSAGE_LOCALIZEPARTREQUEST_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace dcm_pack
{
template <class ContainerAllocator>
struct LocalizePartRequest_
{
  typedef LocalizePartRequest_<ContainerAllocator> Type;

  LocalizePartRequest_()
    : req()  {
    }
  LocalizePartRequest_(const ContainerAllocator& _alloc)
    : req(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _req_type;
  _req_type req;





  typedef boost::shared_ptr< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> const> ConstPtr;

}; // struct LocalizePartRequest_

typedef ::dcm_pack::LocalizePartRequest_<std::allocator<void> > LocalizePartRequest;

typedef boost::shared_ptr< ::dcm_pack::LocalizePartRequest > LocalizePartRequestPtr;
typedef boost::shared_ptr< ::dcm_pack::LocalizePartRequest const> LocalizePartRequestConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::dcm_pack::LocalizePartRequest_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::dcm_pack::LocalizePartRequest_<ContainerAllocator1> & lhs, const ::dcm_pack::LocalizePartRequest_<ContainerAllocator2> & rhs)
{
  return lhs.req == rhs.req;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::dcm_pack::LocalizePartRequest_<ContainerAllocator1> & lhs, const ::dcm_pack::LocalizePartRequest_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace dcm_pack

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "b8dc53fbc3707f169aa5dbf7b19d2567";
  }

  static const char* value(const ::dcm_pack::LocalizePartRequest_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xb8dc53fbc3707f16ULL;
  static const uint64_t static_value2 = 0x9aa5dbf7b19d2567ULL;
};

template<class ContainerAllocator>
struct DataType< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "dcm_pack/LocalizePartRequest";
  }

  static const char* value(const ::dcm_pack::LocalizePartRequest_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
{
  static const char* value()
  {
    return "#request\n"
"string req\n"
;
  }

  static const char* value(const ::dcm_pack::LocalizePartRequest_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.req);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct LocalizePartRequest_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::dcm_pack::LocalizePartRequest_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::dcm_pack::LocalizePartRequest_<ContainerAllocator>& v)
  {
    s << indent << "req: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.req);
  }
};

} // namespace message_operations
} // namespace ros

#endif // DCM_PACK_MESSAGE_LOCALIZEPARTREQUEST_H
