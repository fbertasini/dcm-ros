// Auto-generated. Do not edit!

// (in-package dcm_pack.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class ResultReadyRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.model = null;
      this.score = null;
      this.position1 = null;
      this.position2 = null;
      this.position3 = null;
      this.rotation1 = null;
      this.rotation2 = null;
      this.rotation3 = null;
      this.rotation4 = null;
    }
    else {
      if (initObj.hasOwnProperty('model')) {
        this.model = initObj.model
      }
      else {
        this.model = 0;
      }
      if (initObj.hasOwnProperty('score')) {
        this.score = initObj.score
      }
      else {
        this.score = 0.0;
      }
      if (initObj.hasOwnProperty('position1')) {
        this.position1 = initObj.position1
      }
      else {
        this.position1 = 0.0;
      }
      if (initObj.hasOwnProperty('position2')) {
        this.position2 = initObj.position2
      }
      else {
        this.position2 = 0.0;
      }
      if (initObj.hasOwnProperty('position3')) {
        this.position3 = initObj.position3
      }
      else {
        this.position3 = 0.0;
      }
      if (initObj.hasOwnProperty('rotation1')) {
        this.rotation1 = initObj.rotation1
      }
      else {
        this.rotation1 = 0.0;
      }
      if (initObj.hasOwnProperty('rotation2')) {
        this.rotation2 = initObj.rotation2
      }
      else {
        this.rotation2 = 0.0;
      }
      if (initObj.hasOwnProperty('rotation3')) {
        this.rotation3 = initObj.rotation3
      }
      else {
        this.rotation3 = 0.0;
      }
      if (initObj.hasOwnProperty('rotation4')) {
        this.rotation4 = initObj.rotation4
      }
      else {
        this.rotation4 = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ResultReadyRequest
    // Serialize message field [model]
    bufferOffset = _serializer.int64(obj.model, buffer, bufferOffset);
    // Serialize message field [score]
    bufferOffset = _serializer.float64(obj.score, buffer, bufferOffset);
    // Serialize message field [position1]
    bufferOffset = _serializer.float64(obj.position1, buffer, bufferOffset);
    // Serialize message field [position2]
    bufferOffset = _serializer.float64(obj.position2, buffer, bufferOffset);
    // Serialize message field [position3]
    bufferOffset = _serializer.float64(obj.position3, buffer, bufferOffset);
    // Serialize message field [rotation1]
    bufferOffset = _serializer.float64(obj.rotation1, buffer, bufferOffset);
    // Serialize message field [rotation2]
    bufferOffset = _serializer.float64(obj.rotation2, buffer, bufferOffset);
    // Serialize message field [rotation3]
    bufferOffset = _serializer.float64(obj.rotation3, buffer, bufferOffset);
    // Serialize message field [rotation4]
    bufferOffset = _serializer.float64(obj.rotation4, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ResultReadyRequest
    let len;
    let data = new ResultReadyRequest(null);
    // Deserialize message field [model]
    data.model = _deserializer.int64(buffer, bufferOffset);
    // Deserialize message field [score]
    data.score = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [position1]
    data.position1 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [position2]
    data.position2 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [position3]
    data.position3 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [rotation1]
    data.rotation1 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [rotation2]
    data.rotation2 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [rotation3]
    data.rotation3 = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [rotation4]
    data.rotation4 = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 72;
  }

  static datatype() {
    // Returns string type for a service object
    return 'dcm_pack/ResultReadyRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '3e9f71e607287c8ee25e31e6daf68630';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #request
    int64 model
    float64 score
    
    float64 position1
    float64 position2
    float64 position3
    
    float64 rotation1
    float64 rotation2
    float64 rotation3
    float64 rotation4
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ResultReadyRequest(null);
    if (msg.model !== undefined) {
      resolved.model = msg.model;
    }
    else {
      resolved.model = 0
    }

    if (msg.score !== undefined) {
      resolved.score = msg.score;
    }
    else {
      resolved.score = 0.0
    }

    if (msg.position1 !== undefined) {
      resolved.position1 = msg.position1;
    }
    else {
      resolved.position1 = 0.0
    }

    if (msg.position2 !== undefined) {
      resolved.position2 = msg.position2;
    }
    else {
      resolved.position2 = 0.0
    }

    if (msg.position3 !== undefined) {
      resolved.position3 = msg.position3;
    }
    else {
      resolved.position3 = 0.0
    }

    if (msg.rotation1 !== undefined) {
      resolved.rotation1 = msg.rotation1;
    }
    else {
      resolved.rotation1 = 0.0
    }

    if (msg.rotation2 !== undefined) {
      resolved.rotation2 = msg.rotation2;
    }
    else {
      resolved.rotation2 = 0.0
    }

    if (msg.rotation3 !== undefined) {
      resolved.rotation3 = msg.rotation3;
    }
    else {
      resolved.rotation3 = 0.0
    }

    if (msg.rotation4 !== undefined) {
      resolved.rotation4 = msg.rotation4;
    }
    else {
      resolved.rotation4 = 0.0
    }

    return resolved;
    }
};

class ResultReadyResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.res = null;
    }
    else {
      if (initObj.hasOwnProperty('res')) {
        this.res = initObj.res
      }
      else {
        this.res = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ResultReadyResponse
    // Serialize message field [res]
    bufferOffset = _serializer.int64(obj.res, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ResultReadyResponse
    let len;
    let data = new ResultReadyResponse(null);
    // Deserialize message field [res]
    data.res = _deserializer.int64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 8;
  }

  static datatype() {
    // Returns string type for a service object
    return 'dcm_pack/ResultReadyResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '3bc8d07e32bae50e58fb4f23232e6b36';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #response
    int64 res
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ResultReadyResponse(null);
    if (msg.res !== undefined) {
      resolved.res = msg.res;
    }
    else {
      resolved.res = 0
    }

    return resolved;
    }
};

module.exports = {
  Request: ResultReadyRequest,
  Response: ResultReadyResponse,
  md5sum() { return '3a9c623e27ff0ffa98326331cfb5181f'; },
  datatype() { return 'dcm_pack/ResultReady'; }
};
