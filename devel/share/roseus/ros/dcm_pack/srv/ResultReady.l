;; Auto-generated. Do not edit!


(when (boundp 'dcm_pack::ResultReady)
  (if (not (find-package "DCM_PACK"))
    (make-package "DCM_PACK"))
  (shadow 'ResultReady (find-package "DCM_PACK")))
(unless (find-package "DCM_PACK::RESULTREADY")
  (make-package "DCM_PACK::RESULTREADY"))
(unless (find-package "DCM_PACK::RESULTREADYREQUEST")
  (make-package "DCM_PACK::RESULTREADYREQUEST"))
(unless (find-package "DCM_PACK::RESULTREADYRESPONSE")
  (make-package "DCM_PACK::RESULTREADYRESPONSE"))

(in-package "ROS")





(defclass dcm_pack::ResultReadyRequest
  :super ros::object
  :slots (_model _score _position1 _position2 _position3 _rotation1 _rotation2 _rotation3 _rotation4 ))

(defmethod dcm_pack::ResultReadyRequest
  (:init
   (&key
    ((:model __model) 0)
    ((:score __score) 0.0)
    ((:position1 __position1) 0.0)
    ((:position2 __position2) 0.0)
    ((:position3 __position3) 0.0)
    ((:rotation1 __rotation1) 0.0)
    ((:rotation2 __rotation2) 0.0)
    ((:rotation3 __rotation3) 0.0)
    ((:rotation4 __rotation4) 0.0)
    )
   (send-super :init)
   (setq _model (round __model))
   (setq _score (float __score))
   (setq _position1 (float __position1))
   (setq _position2 (float __position2))
   (setq _position3 (float __position3))
   (setq _rotation1 (float __rotation1))
   (setq _rotation2 (float __rotation2))
   (setq _rotation3 (float __rotation3))
   (setq _rotation4 (float __rotation4))
   self)
  (:model
   (&optional __model)
   (if __model (setq _model __model)) _model)
  (:score
   (&optional __score)
   (if __score (setq _score __score)) _score)
  (:position1
   (&optional __position1)
   (if __position1 (setq _position1 __position1)) _position1)
  (:position2
   (&optional __position2)
   (if __position2 (setq _position2 __position2)) _position2)
  (:position3
   (&optional __position3)
   (if __position3 (setq _position3 __position3)) _position3)
  (:rotation1
   (&optional __rotation1)
   (if __rotation1 (setq _rotation1 __rotation1)) _rotation1)
  (:rotation2
   (&optional __rotation2)
   (if __rotation2 (setq _rotation2 __rotation2)) _rotation2)
  (:rotation3
   (&optional __rotation3)
   (if __rotation3 (setq _rotation3 __rotation3)) _rotation3)
  (:rotation4
   (&optional __rotation4)
   (if __rotation4 (setq _rotation4 __rotation4)) _rotation4)
  (:serialization-length
   ()
   (+
    ;; int64 _model
    8
    ;; float64 _score
    8
    ;; float64 _position1
    8
    ;; float64 _position2
    8
    ;; float64 _position3
    8
    ;; float64 _rotation1
    8
    ;; float64 _rotation2
    8
    ;; float64 _rotation3
    8
    ;; float64 _rotation4
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _model
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _model (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _model) (= (length (_model . bv)) 2)) ;; bignum
              (write-long (ash (elt (_model . bv) 0) 0) s)
              (write-long (ash (elt (_model . bv) 1) -1) s))
             ((and (class _model) (= (length (_model . bv)) 1)) ;; big1
              (write-long (elt (_model . bv) 0) s)
              (write-long (if (>= _model 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _model s)(write-long (if (>= _model 0) 0 #xffffffff) s)))
     ;; float64 _score
       (sys::poke _score (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _position1
       (sys::poke _position1 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _position2
       (sys::poke _position2 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _position3
       (sys::poke _position3 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rotation1
       (sys::poke _rotation1 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rotation2
       (sys::poke _rotation2 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rotation3
       (sys::poke _rotation3 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _rotation4
       (sys::poke _rotation4 (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _model
#+(or :alpha :irix6 :x86_64)
      (setf _model (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _model (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; float64 _score
     (setq _score (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _position1
     (setq _position1 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _position2
     (setq _position2 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _position3
     (setq _position3 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rotation1
     (setq _rotation1 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rotation2
     (setq _rotation2 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rotation3
     (setq _rotation3 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _rotation4
     (setq _rotation4 (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(defclass dcm_pack::ResultReadyResponse
  :super ros::object
  :slots (_res ))

(defmethod dcm_pack::ResultReadyResponse
  (:init
   (&key
    ((:res __res) 0)
    )
   (send-super :init)
   (setq _res (round __res))
   self)
  (:res
   (&optional __res)
   (if __res (setq _res __res)) _res)
  (:serialization-length
   ()
   (+
    ;; int64 _res
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _res
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _res (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _res) (= (length (_res . bv)) 2)) ;; bignum
              (write-long (ash (elt (_res . bv) 0) 0) s)
              (write-long (ash (elt (_res . bv) 1) -1) s))
             ((and (class _res) (= (length (_res . bv)) 1)) ;; big1
              (write-long (elt (_res . bv) 0) s)
              (write-long (if (>= _res 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _res s)(write-long (if (>= _res 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _res
#+(or :alpha :irix6 :x86_64)
      (setf _res (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _res (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(defclass dcm_pack::ResultReady
  :super ros::object
  :slots ())

(setf (get dcm_pack::ResultReady :md5sum-) "3a9c623e27ff0ffa98326331cfb5181f")
(setf (get dcm_pack::ResultReady :datatype-) "dcm_pack/ResultReady")
(setf (get dcm_pack::ResultReady :request) dcm_pack::ResultReadyRequest)
(setf (get dcm_pack::ResultReady :response) dcm_pack::ResultReadyResponse)

(defmethod dcm_pack::ResultReadyRequest
  (:response () (instance dcm_pack::ResultReadyResponse :init)))

(setf (get dcm_pack::ResultReadyRequest :md5sum-) "3a9c623e27ff0ffa98326331cfb5181f")
(setf (get dcm_pack::ResultReadyRequest :datatype-) "dcm_pack/ResultReadyRequest")
(setf (get dcm_pack::ResultReadyRequest :definition-)
      "#request
int64 model
float64 score

float64 position1
float64 position2
float64 position3

float64 rotation1
float64 rotation2
float64 rotation3
float64 rotation4
---
#response
int64 res

")

(setf (get dcm_pack::ResultReadyResponse :md5sum-) "3a9c623e27ff0ffa98326331cfb5181f")
(setf (get dcm_pack::ResultReadyResponse :datatype-) "dcm_pack/ResultReadyResponse")
(setf (get dcm_pack::ResultReadyResponse :definition-)
      "#request
int64 model
float64 score

float64 position1
float64 position2
float64 position3

float64 rotation1
float64 rotation2
float64 rotation3
float64 rotation4
---
#response
int64 res

")



(provide :dcm_pack/ResultReady "3a9c623e27ff0ffa98326331cfb5181f")


