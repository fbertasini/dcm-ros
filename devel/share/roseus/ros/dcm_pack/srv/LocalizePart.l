;; Auto-generated. Do not edit!


(when (boundp 'dcm_pack::LocalizePart)
  (if (not (find-package "DCM_PACK"))
    (make-package "DCM_PACK"))
  (shadow 'LocalizePart (find-package "DCM_PACK")))
(unless (find-package "DCM_PACK::LOCALIZEPART")
  (make-package "DCM_PACK::LOCALIZEPART"))
(unless (find-package "DCM_PACK::LOCALIZEPARTREQUEST")
  (make-package "DCM_PACK::LOCALIZEPARTREQUEST"))
(unless (find-package "DCM_PACK::LOCALIZEPARTRESPONSE")
  (make-package "DCM_PACK::LOCALIZEPARTRESPONSE"))

(in-package "ROS")





(defclass dcm_pack::LocalizePartRequest
  :super ros::object
  :slots (_req ))

(defmethod dcm_pack::LocalizePartRequest
  (:init
   (&key
    ((:req __req) "")
    )
   (send-super :init)
   (setq _req (string __req))
   self)
  (:req
   (&optional __req)
   (if __req (setq _req __req)) _req)
  (:serialization-length
   ()
   (+
    ;; string _req
    4 (length _req)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _req
       (write-long (length _req) s) (princ _req s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _req
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _req (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass dcm_pack::LocalizePartResponse
  :super ros::object
  :slots (_res ))

(defmethod dcm_pack::LocalizePartResponse
  (:init
   (&key
    ((:res __res) 0)
    )
   (send-super :init)
   (setq _res (round __res))
   self)
  (:res
   (&optional __res)
   (if __res (setq _res __res)) _res)
  (:serialization-length
   ()
   (+
    ;; int64 _res
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int64 _res
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _res (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _res) (= (length (_res . bv)) 2)) ;; bignum
              (write-long (ash (elt (_res . bv) 0) 0) s)
              (write-long (ash (elt (_res . bv) 1) -1) s))
             ((and (class _res) (= (length (_res . bv)) 1)) ;; big1
              (write-long (elt (_res . bv) 0) s)
              (write-long (if (>= _res 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _res s)(write-long (if (>= _res 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int64 _res
#+(or :alpha :irix6 :x86_64)
      (setf _res (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _res (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(defclass dcm_pack::LocalizePart
  :super ros::object
  :slots ())

(setf (get dcm_pack::LocalizePart :md5sum-) "7f56d8b2c90857d3c86229b97eb0794c")
(setf (get dcm_pack::LocalizePart :datatype-) "dcm_pack/LocalizePart")
(setf (get dcm_pack::LocalizePart :request) dcm_pack::LocalizePartRequest)
(setf (get dcm_pack::LocalizePart :response) dcm_pack::LocalizePartResponse)

(defmethod dcm_pack::LocalizePartRequest
  (:response () (instance dcm_pack::LocalizePartResponse :init)))

(setf (get dcm_pack::LocalizePartRequest :md5sum-) "7f56d8b2c90857d3c86229b97eb0794c")
(setf (get dcm_pack::LocalizePartRequest :datatype-) "dcm_pack/LocalizePartRequest")
(setf (get dcm_pack::LocalizePartRequest :definition-)
      "#request
string req
---
#response
int64 res

")

(setf (get dcm_pack::LocalizePartResponse :md5sum-) "7f56d8b2c90857d3c86229b97eb0794c")
(setf (get dcm_pack::LocalizePartResponse :datatype-) "dcm_pack/LocalizePartResponse")
(setf (get dcm_pack::LocalizePartResponse :definition-)
      "#request
string req
---
#response
int64 res

")



(provide :dcm_pack/LocalizePart "7f56d8b2c90857d3c86229b97eb0794c")


