
(cl:in-package :asdf)

(defsystem "dcm_pack-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "ImageReady" :depends-on ("_package_ImageReady"))
    (:file "_package_ImageReady" :depends-on ("_package"))
    (:file "ResultReady" :depends-on ("_package_ResultReady"))
    (:file "_package_ResultReady" :depends-on ("_package"))
  ))