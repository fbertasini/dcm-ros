; Auto-generated. Do not edit!


(cl:in-package dcm_pack-srv)


;//! \htmlinclude LocalizePart-request.msg.html

(cl:defclass <LocalizePart-request> (roslisp-msg-protocol:ros-message)
  ((req
    :reader req
    :initarg :req
    :type cl:string
    :initform ""))
)

(cl:defclass LocalizePart-request (<LocalizePart-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <LocalizePart-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'LocalizePart-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dcm_pack-srv:<LocalizePart-request> is deprecated: use dcm_pack-srv:LocalizePart-request instead.")))

(cl:ensure-generic-function 'req-val :lambda-list '(m))
(cl:defmethod req-val ((m <LocalizePart-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dcm_pack-srv:req-val is deprecated.  Use dcm_pack-srv:req instead.")
  (req m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <LocalizePart-request>) ostream)
  "Serializes a message object of type '<LocalizePart-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'req))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'req))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <LocalizePart-request>) istream)
  "Deserializes a message object of type '<LocalizePart-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'req) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'req) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<LocalizePart-request>)))
  "Returns string type for a service object of type '<LocalizePart-request>"
  "dcm_pack/LocalizePartRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LocalizePart-request)))
  "Returns string type for a service object of type 'LocalizePart-request"
  "dcm_pack/LocalizePartRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<LocalizePart-request>)))
  "Returns md5sum for a message object of type '<LocalizePart-request>"
  "7f56d8b2c90857d3c86229b97eb0794c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'LocalizePart-request)))
  "Returns md5sum for a message object of type 'LocalizePart-request"
  "7f56d8b2c90857d3c86229b97eb0794c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<LocalizePart-request>)))
  "Returns full string definition for message of type '<LocalizePart-request>"
  (cl:format cl:nil "#request~%string req~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'LocalizePart-request)))
  "Returns full string definition for message of type 'LocalizePart-request"
  (cl:format cl:nil "#request~%string req~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <LocalizePart-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'req))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <LocalizePart-request>))
  "Converts a ROS message object to a list"
  (cl:list 'LocalizePart-request
    (cl:cons ':req (req msg))
))
;//! \htmlinclude LocalizePart-response.msg.html

(cl:defclass <LocalizePart-response> (roslisp-msg-protocol:ros-message)
  ((res
    :reader res
    :initarg :res
    :type cl:integer
    :initform 0))
)

(cl:defclass LocalizePart-response (<LocalizePart-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <LocalizePart-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'LocalizePart-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name dcm_pack-srv:<LocalizePart-response> is deprecated: use dcm_pack-srv:LocalizePart-response instead.")))

(cl:ensure-generic-function 'res-val :lambda-list '(m))
(cl:defmethod res-val ((m <LocalizePart-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader dcm_pack-srv:res-val is deprecated.  Use dcm_pack-srv:res instead.")
  (res m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <LocalizePart-response>) ostream)
  "Serializes a message object of type '<LocalizePart-response>"
  (cl:let* ((signed (cl:slot-value msg 'res)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <LocalizePart-response>) istream)
  "Deserializes a message object of type '<LocalizePart-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'res) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<LocalizePart-response>)))
  "Returns string type for a service object of type '<LocalizePart-response>"
  "dcm_pack/LocalizePartResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LocalizePart-response)))
  "Returns string type for a service object of type 'LocalizePart-response"
  "dcm_pack/LocalizePartResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<LocalizePart-response>)))
  "Returns md5sum for a message object of type '<LocalizePart-response>"
  "7f56d8b2c90857d3c86229b97eb0794c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'LocalizePart-response)))
  "Returns md5sum for a message object of type 'LocalizePart-response"
  "7f56d8b2c90857d3c86229b97eb0794c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<LocalizePart-response>)))
  "Returns full string definition for message of type '<LocalizePart-response>"
  (cl:format cl:nil "#response~%int64 res~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'LocalizePart-response)))
  "Returns full string definition for message of type 'LocalizePart-response"
  (cl:format cl:nil "#response~%int64 res~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <LocalizePart-response>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <LocalizePart-response>))
  "Converts a ROS message object to a list"
  (cl:list 'LocalizePart-response
    (cl:cons ':res (res msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'LocalizePart)))
  'LocalizePart-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'LocalizePart)))
  'LocalizePart-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LocalizePart)))
  "Returns string type for a service object of type '<LocalizePart>"
  "dcm_pack/LocalizePart")